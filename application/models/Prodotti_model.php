<?php
class Prodotti_model extends CI_Model {
  public function __construct()
  {
    $this->load->database();
  }

  public function get_prodotti()
  {
    $this->db->order_by('pr_ID');
    $query = $this->db->where('pr_eliminato', 0);
    $query = $this->db->get('prodotti');
    return $query->result_array();
  }

  public function get_prodotto($id)
  {
    $query = $this->db->get_where('prodotti', array('pr_ID' => $id));
    return $query->row_array();
  }

  public function create_prodotto($data)
  {
    return $this->db->insert('prodotti', $data);
  }

  public function delete_prodotto($id)
  {
    $this->db->set('pr_eliminato', 1);
    $this->db->where('pr_ID', $id);
    return $this->db->update('prodotti');
  }

  public function update_prodotto($id, $data)
  {
    foreach ($data as $key => $value) {
      $this->db->set($key, $value);
    }
    $this->db->where('pr_ID', $id);
    return $this->db->update('prodotti');
  }

}
