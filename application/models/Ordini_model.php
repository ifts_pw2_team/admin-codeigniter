<?php
class Ordini_model extends CI_Model {
  public function __construct()
  {
    $this->load->database();
  }

  public function get_ordini()
  {
    $query = $this->db->get('ordini');
    return $query->result_array();
  }
  public function get_ordine($id)
  {
    // seleziona info ordine
    $query = $this->db->get_where('ordini', array('or_ID' => $id));
    $infoOrdine = $query->row_array();
    // seleziona info prodotti per ordine
    $this->db->join('riga_ordine', 'pr_ID = ro_ID_prodotto');
    $query = $this->db->get_where('prodotti', array('ro_ID_ordine' => $id));
    $prodotti = $query->result_array();
    $infoOrdine["prodotti"] = $prodotti;

    return $infoOrdine;
  }

  public function update_ordine($id, $data)
  {
    foreach ($data as $key => $value) {
      $this->db->set($key, $value);
    }
    $this->db->where('or_ID', $id);
    return $this->db->update('ordini');
  }

  public function delete_ordine($id)
  {
    $this->db->where('ro_ID_ordine', $id);
    $res1 = $this->db->delete('riga_ordine');

    $this->db->where('or_ID', $id);
    $res2 = $this->db->delete('ordini');

    return $res1 && $res2;
  }
}
