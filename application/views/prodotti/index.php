<div class="row">
  <div class="col-md-12">
    <?php
    $feedback = $this->session->flashdata('feedback');
    if ($feedback): ?>
      <div class="alert alert-success">
        <?php echo $feedback ?>
      </div>
    <?php endif;

    $error_msg = $this->session->flashdata('error_msg');
    if ($error_msg): ?>
      <div class="alert alert-danger">
        <?php echo $error_msg ?>
      </div>
    <?php endif;?>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th>Prezzo(&euro;)</th>
          <th>Opzioni</th>
        </tr>
      </thead>
      <tfoot>

      </tfoot>
      <tbody>
        <?php if (!count($prodotti)): ?>
          <tr class="text-center">
            <td colspan="4">Nessun dato in archivio.</td>
          </tr>
        <?php endif; ?>
        <?php foreach ($prodotti as $key => $prodotto): ?>
          <tr>
            <td><?php echo $prodotto["pr_ID"] ?></td>
            <td><?php echo $prodotto["pr_nome"] ?></td>
            <td><?php echo number_format($prodotto["pr_prezzo"], 2, ",", "."); ?></td>
            <td>
              <a href="<?php echo site_url('prodotti/modifica/'.$prodotto['pr_ID']); ?>" title="Modifica"><i class="fas fa-edit" aria-hidden="true"></i></a>
              <a href="<?php echo site_url('prodotti/elimina/'.$prodotto['pr_ID']); ?>" title="Elimina"><i class="fas fa-trash" aria-hidden="true"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

    <a href="<?php echo site_url('prodotti/nuovo'); ?>" class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i> Crea Prodotto</a>
  </div>
</div>
