<div class="row">
  <div class="col-md-12">
    <?php
    $feedback = $this->session->flashdata('feedback');
    if ($feedback): ?>
      <div class="alert alert-success">
        <?php echo $feedback ?>
      </div>
    <?php endif;

    $error_msg = $this->session->flashdata('error_msg');
    if ($error_msg): ?>
      <div class="alert alert-danger">
        <?php echo $error_msg ?>
      </div>
    <?php endif;?>

    <?php $validation_error = validation_errors(); ?>
    <?php if($validation_error) : ?>
      <div class="alert alert-warning" role="alert">
        <?php echo $validation_error; ?>
      </div>
    <?php endif; ?>

    <?php echo form_open(($mode == 'C') ? 'prodotti/nuovo' : 'prodotti/modifica/'.$infoProdotto['pr_ID']); ?>
        <?php if ($mode == 'E'): ?>
          <div class="form-group">
              <label for="input_id">ID</label>
              <input id="input_id" type="text" class="form-control" name="pr_id" value="<?php echo ($mode == 'E') ? set_value('pr_id', $infoProdotto['pr_ID']) : '' ;?>" disabled/>
          </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="nome-inpt">Nome</label>
            <input id="nome-inpt" type="text" class="form-control" name="pr_nome" value="<?php echo ($mode == 'E') ? set_value('pr_nome', $infoProdotto['pr_nome']) : '' ;?>" required/>
        </div>
        <div class="form-group">
            <label for="descrizione-txt">Descrizione</label>
            <textarea id="descrizione-txt" name="pr_descrizione" rows="10" required style="width: 100%"><?php echo ($mode == 'E') ? trim($infoProdotto['pr_descrizione']) : '' ;?></textarea>
        </div>
        <div class="form-group">
            <label for="prezzo-inpt">Prezzo</label>
            <input id="prezzo-inpt" type="number" class="form-control" name="pr_prezzo"
              value="<?php echo ($mode == 'E') ? set_value('pr_prezzo', $infoProdotto['pr_prezzo']) : '' ;?>" required min="0.00" step="0.01"/>
        </div>
        <a href="<?php echo site_url('prodotti') ?>" class="btn btn-default"><i class="fas fa-times"></i> Annulla</a>
        <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Salva</button>
    <?php echo form_close(); ?>

  </div>
</div>
