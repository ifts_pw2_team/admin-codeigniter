<div class="row">
  <div class="col-xs-12">
    <?php
    $feedback = $this->session->flashdata('feedback');
    if ($feedback): ?>
      <div class="alert alert-success">
        <?php echo $feedback ?>
      </div>
    <?php endif;

    $error_msg = $this->session->flashdata('error_msg');
    if ($error_msg): ?>
      <div class="alert alert-danger">
        <?php echo $error_msg ?>
      </div>
    <?php endif;?>

    <?php $validation_error = validation_errors(); ?>
    <?php if($validation_error) : ?>
      <div class="alert alert-warning" role="alert">
        <?php echo $validation_error; ?>
      </div>
    <?php endif; ?>

    <?php echo form_open('ordini/modifica/'.$infoOrdine['or_ID']); ?>
      <table class="table">
        <tbody>
          <tr>
            <td>ID</td>
            <td><?php echo $infoOrdine['or_ID'] ?></td>
          </tr>
          <tr>
            <td>Stato</td>
            <td>
              <select class="form-control" name="or_stato" id="stato-select">
                <?php foreach ($statiOrdine as $codice => $desc): ?>
                  <option value="<?php echo $codice ?>" <?php echo ($codice == $infoOrdine['or_stato']) ? "selected" : "" ; ?>><?php echo $desc ?></option>
                <?php endforeach; ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>Importo</td>
            <td>
              &euro; <?php echo number_format($infoOrdine["or_importo"], 2, ",", ".") ?>
            </td>
          </tr>
          <tr>
            <td>Data/Ora</td>
            <td>
              <?php echo $infoOrdine["or_orario"] ?>
            </td>
          </tr>
          <tr>
            <td>Ora consegna</td>
            <td>
              <?php echo $infoOrdine["or_ora_consegna"] ?>
            </td>
          </tr>
          <tr>
            <td>Telefono</td>
            <td>
              <?php echo $infoOrdine["or_telefono"] ?>
            </td>
          </tr>
          <tr>
            <td>Email</td>
            <td>
              <?php echo $infoOrdine["or_email"] ?>
            </td>
          </tr>
          <tr>
            <td>Indirizzo</td>
            <td>
              via <?php echo $infoOrdine["or_via"] ?>, <?php echo $infoOrdine["or_numero_civico"] ?> - <?php echo $infoOrdine["or_citta"] ?>
            </td>
          </tr>
        </tbody>
      </table>
        <a href="<?php echo site_url('ordini') ?>" class="btn btn-default"><i class="fas fa-times"></i> Annulla</a>
        <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Salva</button>
    <?php echo form_close(); ?>
    <h3>Elenco prodotti</h3>
    <table class="table table-">
      <thead class="thead-dark">
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th>Prezzo(&euro;)</th>
          <th>Quantit&agrave;</th>
          <th>Visualizza</th>
        </tr>
      </thead>
      <tfoot>

      </tfoot>
      <tbody>
        <?php foreach ($infoOrdine["prodotti"] as $key => $prodotto): ?>
          <tr>
            <td><?php echo $prodotto["pr_ID"] ?></td>
            <td><?php echo $prodotto["pr_nome"] ?></td>
            <td><?php echo number_format($prodotto["pr_prezzo"], 2, ",", "."); ?></td>
            <td><?php echo $prodotto["ro_quantita"] ?></td>
            <td>
              <a href="<?php echo site_url('prodotti/modifica/'.$prodotto['pr_ID']); ?>" title="Visualizza"><i class="fas fa-eye" aria-hidden="true"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
