<div class="row">
  <div class="col-md-12">
    <?php
    $feedback = $this->session->flashdata('feedback');
    if ($feedback): ?>
      <div class="alert alert-success">
        <?php echo $feedback ?>
      </div>
    <?php endif;

    $error_msg = $this->session->flashdata('error_msg');
    if ($error_msg): ?>
      <div class="alert alert-danger">
        <?php echo $error_msg ?>
      </div>
    <?php endif;?>
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>#</th>
          <th>Stato</th>
          <th>Data/Ora</th>
          <th>Consegna</th>
          <th>Telefono Cliente</th>
          <th>Importo(&euro;)</th>
          <th>Opzioni</th>
        </tr>
      </thead>
      <tfoot>

      </tfoot>
      <tbody>
        <?php if (!count($ordini)): ?>
          <tr class="text-center">
            <td colspan="7">Nessun dato in archivio.</td>
          </tr>
        <?php endif; ?>
        <?php foreach ($ordini as $key => $ordine): ?>
          <tr>
            <td><?php echo $ordine["or_ID"] ?></td>
            <td><?php echo $statiOrdine[$ordine["or_stato"]] ?></td>
            <td><?php echo $ordine["or_orario"] ?></td>
            <td><?php echo $ordine["or_ora_consegna"] ?></td>
            <td><?php echo $ordine["or_telefono"] ?></td>
            <td><?php echo number_format($ordine["or_importo"], 2, ",", "."); ?></td>
            <td>
              <a href="<?php echo site_url('ordini/visualizza/'.$ordine['or_ID']); ?>" title="Visualizza/Modifica"><i class="fas fa-eye" aria-hidden="true"></i></a>
              <a href="<?php echo site_url('ordini/elimina/'.$ordine['or_ID']); ?>" title="Elimina"><i class="fas fa-trash" aria-hidden="true"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
