<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

    <title><?php echo $title;?></title>
  </head>
  <body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container">
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
  			<span class="navbar-toggler-icon"></span>
  		</button>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item <?php if($section=='ordini_index'):?>active<?php endif;?>">
						<a href="<?php echo site_url('ordini');?>" class="nav-link"><i class="fas fa-truck"></i> Ordini</a>
					</li>
					<li class="nav-item <?php if($section=='prodotti_index'):?>active<?php endif;?>">
						<a href="<?php echo site_url('prodotti');?>" class="nav-link"><i class="fas fa-list"></i> Prodotti</a>
					</li>
					<li class="nav-item <?php if($section=='prodotti_nuovo'):?>active<?php endif;?>">
						<a href="<?php echo site_url('prodotti/nuovo');?>" class="nav-link"><i class="fas fa-plus"></i> Nuovo Prodotto</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
  <div class="container content">
    <h1 class="text-center"><?php echo $title;?></h1>
