<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordini extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('ordini_model');
    $this->statiOrdine = array(
      '1' => "Ricevuto",
      '2' => "In elaborazione",
      '3' => "In consegna",
      '4' => "Completato"
    );
  }
	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		$data = array();
    $data["title"] = "Lista ordini";
    $data["section"] = "ordini_index";
    $data['statiOrdine'] = $this->statiOrdine;
    $data['ordini'] = $this->ordini_model->get_ordini();

		//Passo i dati alla vista
		$this->load->view('common/header', $data);
		$this->load->view('ordini/index', $data);
		$this->load->view('common/footer');
	}

  public function visualizza($id)
  {
    $this->load->helper('form');
    $this->load->library('form_validation');
		$data = array();
    $data["title"] = "Visualizzazione/Modifica ordine";
    $data["section"] = "ordini_visualizza";
    $data['statiOrdine'] = $this->statiOrdine;
    $data['infoOrdine'] = $this->ordini_model->get_ordine($id);
    if (!count($data['infoOrdine']['prodotti'])) {
      redirect('ordini');
    }
    // print_r($data['infoOrdine']);
    // return false;
    $this->load->view('common/header',$data);
    $this->load->view('ordini/dettaglio', $data);
    $this->load->view('common/footer');
  }

  public function modifica($id)
  {
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('or_stato','Stato ordine','required');

    if($this->form_validation->run() === FALSE) {
      //Validazione fallita o form non inviato
      redirect('ordini/visualizza/'.$id);
    }
    else {
      //Validazione ok
      $data = array(
        'or_stato' => intval($this->input->post('or_stato')),
      );
      $res = $this->ordini_model->update_ordine($id, $data);
      if ($res === true) {
        $this->session->set_flashdata('feedback', 'Ordine aggiornato con successo.');
        redirect('ordini');
      } else {
        $this->session->set_flashdata('error_msg', 'Errore!('.serialize($res).')');
        redirect('ordini/visualizza/'.$id);
      }
    }
  }

  public function elimina($id)
  {
    $res = $this->ordini_model->delete_ordine($id);
    // var_dump($res);
    if ($res === true) {
      $this->session->set_flashdata('feedback', 'Ordine Eliminato con successo.');
    } else {
      $this->session->set_flashdata('error_msg', 'Errore!('.serialize($res).')');
    }
    redirect('ordini');
  }
}
