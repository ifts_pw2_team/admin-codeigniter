<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodotti extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('prodotti_model');
  }
	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		$data = array();
    $data["title"] = "Lista prodotti";
    $data["section"] = "prodotti_index";
    $data['prodotti'] = $this->prodotti_model->get_prodotti();

		//Passo i dati alla vista
		$this->load->view('common/header', $data);
		$this->load->view('prodotti/index', $data);
		$this->load->view('common/footer');
	}

  public function nuovo()
  {
    $this->load->helper('form');
    $this->load->library('form_validation');

		$data = array();
    $data["title"] = "Creazione nuovo prodotto";
    $data["section"] = "prodotti_nuovo";
    $data['mode'] = 'C';
    $infoProdotto = array();

    $this->form_validation->set_rules('pr_nome','Nome prodotto','required|min_length[3]|max_length[100]');
    $this->form_validation->set_rules('pr_descrizione','Descrizione','required|min_length[10]|max_length[500]');
    $this->form_validation->set_rules('pr_prezzo','Prezzo','required');

    if($this->form_validation->run() === FALSE) {
      //Validazione fallita o form non inviato
      $this->load->view('common/header',$data);
      $this->load->view('prodotti/dettaglio', $data);
      $this->load->view('common/footer');
    }
    else {
      //Validazione ok
      $data = array(
        'pr_nome' => $this->input->post('pr_nome'),
        'pr_descrizione' => $this->input->post('pr_descrizione'),
        'pr_prezzo' => floatval($this->input->post('pr_prezzo')),
      );
      $res = $this->prodotti_model->create_prodotto($data);

      if ($res === true) {
        $this->session->set_flashdata('feedback', 'Prodotto creato con successo.');
        redirect('prodotti/index');
      } else {
        $this->session->set_flashdata('error_msg', 'Errore!('.serialize($res).')');
        redirect('prodotti/nuovo');
      }

    }
  }

  public function modifica($id)
  {
    $this->load->helper('form');
    $this->load->library('form_validation');

		$data = array();
    $data["title"] = "Visualizzazione/Modifica prodotto";
    $data["section"] = "prodotti_modifica";
    $data['mode'] = 'E';
    $data['infoProdotto'] = $this->prodotti_model->get_prodotto($id);
    if (!$data['infoProdotto']) {
      redirect('prodotti');
    }

    $this->form_validation->set_rules('pr_nome','Nome prodotto','required|min_length[3]|max_length[100]');
    $this->form_validation->set_rules('pr_descrizione','Descrizione','required|min_length[10]|max_length[500]');
    $this->form_validation->set_rules('pr_prezzo','Prezzo','required');

    if($this->form_validation->run() === FALSE) {
      //Validazione fallita o form non inviato
      $this->load->view('common/header',$data);
      $this->load->view('prodotti/dettaglio', $data);
      $this->load->view('common/footer');
    }
    else {
      //Validazione ok
      $data = array(
        'pr_nome' => $this->input->post('pr_nome'),
        'pr_descrizione' => $this->input->post('pr_descrizione'),
        'pr_prezzo' => floatval($this->input->post('pr_prezzo')),
      );
      $res = $this->prodotti_model->update_prodotto($id, $data);
      if ($res === true) {
        $this->session->set_flashdata('feedback', 'Prodotto modificato con successo.');
        redirect('prodotti/index');
      } else {
        $this->session->set_flashdata('error_msg', 'Errore!('.serialize($res).')');
        redirect('prodotti/modifica/'.$id);
      }

    }
  }

  public function elimina($id)
  {
    $res = $this->prodotti_model->delete_prodotto($id);
    var_dump($res);
    if ($res === true) {
      $this->session->set_flashdata('feedback', 'Prodotto Eliminato con successo.');
    } else {
      $this->session->set_flashdata('error_msg', 'Errore!('.serialize($res).')');
    }
    redirect('prodotti');
  }
}
